//
//  AppDelegate.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var rootWireframe: Wireframe?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("[DEBUG INFO] - Zengo can reach the internet: \(ReachabilityService.isNetworkReachable ? "yup" : "nay")")
        prepareUserInterFace()
        return true
    }
}

//MARK: AppDelegate (private)

private extension AppDelegate {
    
    func prepareUserInterFace() {
        
        if window == nil  {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        
        rootWireframe = SplashWireframe(completion: { [unowned self] () -> () in
            self.displayLandingWireframe()
        })
        
        window?.rootViewController = rootWireframe?.viewController()
        window?.makeKeyAndVisible()
    }
    
    func displayLandingWireframe() {
        rootWireframe = MainWireframe()
        window?.rootViewController = rootWireframe?.viewController()
    }
}

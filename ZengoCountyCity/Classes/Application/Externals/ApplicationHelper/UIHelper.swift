//
//  UIHelper.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

//MARK: UIViewController

extension UIViewController {
    
    static func setBackButton(_ title : String, target : UIViewController) {
        var image = UIImage(named: "back")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        image = image?.resizableImage(withCapInsets: UIEdgeInsetsMake(11, image!.size.width - 1, 0, 0))
        
        if #available(iOS 9.0, *) {
            let appearance = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationController.self])
            appearance.setBackButtonBackgroundImage(image, for: UIControlState(), barMetrics: UIBarMetrics.default)
        } else {
            let appearance = UIBarButtonItem.appearance()
            appearance.setBackButtonBackgroundImage(image, for: UIControlState(), barMetrics: UIBarMetrics.default)
        }
        
        target.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
        target.navigationController?.navigationBar.tintColor = UIColor.white
    }
}

//MARK: UIColor

extension UIColor {
    
    fileprivate static func colorWith(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    
    static func appGreenColor() -> UIColor {
        return UIColor(netHex: 0x06a15e)
    }
    
    static func appRedColor() -> UIColor {
        return UIColor(netHex: 0xe6324d)
    }

    static func appYellowColor() -> UIColor {
        return UIColor(netHex: 0xfc8905)
    }
    
    static func appPurpleColor() -> UIColor {
        return UIColor(netHex: 0xb03aac)
    }

    static func appBlueColor() -> UIColor {
        return UIColor(netHex: 0x084659)
    }

    static func appWhiteColor() -> UIColor {
        return UIColor(netHex: 0xFCFCFC)
    }
    
    static func appBlackColor() -> UIColor {
        return UIColor(netHex: 0x242424)
    }
    
    static func appGrayColor() -> UIColor {
        return UIColor(netHex: 0x9F9F9F)
    }
    
    convenience init(netHex: Int) {
        self.init(red: CGFloat((netHex >> 16) & 0xff) / 255.0, green: CGFloat((netHex >> 8) & 0xff) / 255.0, blue: CGFloat(netHex & 0xff) / 255.0, alpha: 1.0)
    }
}

//MARK: UIFont

extension UIFont {
    
    static func applicationFontWithSize(_ size : CGFloat) -> UIFont {
        return UIFont(name: "TitilliumWeb-Regular", size: size)!
    }
    
    static func applicationBoldFontWithSize(_ size : CGFloat) -> UIFont {
        return UIFont(name: "TitilliumWeb-Bold", size: size)!
    }
}

//MARK: UINavigationItem

extension UINavigationItem {
    
    func applyNavigationItem(withImageName name: String) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: name)
        imageView.image = image
        self.titleView = imageView
    }
}

//MARK: UIView

extension UIView {
    
    func applyShadow(_ shadowOffset: CGSize) {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 1.0
    }
}

//
//  MainWireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

enum Screen : String {
    case CountyList, CityList
}

protocol MainRouter : class {
    func pushScreen(_ type: Screen, withNavigationController navigationController: UINavigationController)
    func pushScreen(_ type: Screen, withNavigationController navigationController: UINavigationController, payload: Any?)
}

class MainWireframe {
    
    fileprivate var mainViewController = UIViewController()
    fileprivate var countyListWireframe: CountyListWireframe?
    fileprivate var cityListWireframe: CityListWireframe?
    fileprivate var centerWireframe: Wireframe?
    
    init() {
        countyListWireframe = CountyListWireframe(completion: .none)
        countyListWireframe?.router = self
        setCenterWireframe(countyListWireframe!, embeddedInNavigation: true)
    }
    
    func push(_ navigationController: UINavigationController, wireFrame: Wireframe) {
        navigationController.pushViewController(wireFrame.viewController(), animated: true)
    }
}

//MARK: MainWireframe (private)

private extension MainWireframe {

    func setCenterWireframe(_ wireframe: Wireframe, embeddedInNavigation: Bool) {
        centerWireframe = wireframe
        mainViewController = embeddedInNavigation ? UINavigationController(rootViewController: (centerWireframe?.viewController())!) : (centerWireframe?.viewController())!
    }
}

//MARK: Wireframe

extension MainWireframe : Wireframe {
    
    func viewController() -> UIViewController {
        return mainViewController
    }
}

//MARK: MainRouter

extension MainWireframe : MainRouter {
    
    func pushScreen(_ type : Screen, withNavigationController navigationController: UINavigationController) {
        self.pushScreen(type, withNavigationController: navigationController, payload: Optional.none)
    }
    
    func pushScreen(_ type: Screen, withNavigationController navigationController: UINavigationController, payload: Any?) {
        var wireFrame: Wireframe! = Optional.none
        
        switch type {
        case .CountyList:
            countyListWireframe = CountyListWireframe(completion: .none)
            countyListWireframe?.router = self
            wireFrame = countyListWireframe
        case .CityList:
            cityListWireframe = CityListWireframe(completion: .none)
            cityListWireframe?.router = self
            wireFrame = cityListWireframe
        }
        
        push(navigationController, wireFrame: wireFrame)
        
        if let payload = payload, let wireFrame = wireFrame as? PayloadHandler {
            wireFrame.handlePayload(payload)
        }
    }
}

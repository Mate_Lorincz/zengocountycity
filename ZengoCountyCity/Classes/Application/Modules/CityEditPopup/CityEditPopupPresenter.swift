//
//  CityEditPopupPresenter.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CityEditPopupEventHandler : class {
    func actionButtonTapped(cityName: String)
    func dismiss()
}

class CityEditPopupPresenter {
    
    fileprivate weak var view: CityEditPopupView?
    fileprivate weak var router: CityEditPopupRouter?
    fileprivate let interactor = CityEditPopupInteractor()

    var county: County?
    
    var city: City? {
        didSet {
            if let city = city {
                view?.cityName = city.name
            }
        }
    }
    
    init(view: CityEditPopupView, router: CityEditPopupRouter) {
        self.view = view
        self.router = router
        interactor.presenter = self
    }
}

//MARK: CityEditPopupEventHandler

extension CityEditPopupPresenter : CityEditPopupEventHandler {
    
    func actionButtonTapped(cityName: String) {
        LoadingIndicator.presentLoadingIndicator()
    
        if let city = city {
            interactor.modifyCity(cityName, cityId: city.id)
        }
        else {
            guard let county = county else {
                return
            }
            
            interactor.createCity(cityName, countyId: county.id)
        }
    }

    func dismiss() {
        router?.dismiss(city: .none)
    }
}

//MARK: CityEditPopupInteractorResult

extension CityEditPopupPresenter : CityEditPopupInteractorResult {
    
    func modifyActionFinishedWithError(_ error: String) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.showAlert(error)
    }
    
    func modifyActionFinishedWithResult(_ result: City) {
        LoadingIndicator.dismissLoadingIndicator()
        router?.dismiss(city: result)
    }
    
    func createActionFinishedWithError(_ error: String) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.showAlert(error)
    }
    
    func createActionFinishedWithResult(_ result: City) {
        LoadingIndicator.dismissLoadingIndicator()
        router?.dismiss(city: result)
    }
}


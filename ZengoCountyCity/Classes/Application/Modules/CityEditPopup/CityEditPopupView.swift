//
//  CityEditPopupView.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CityEditPopupView : class {
    var cityName: String? { get set }
    
    func showAlert(_ message: String)
}

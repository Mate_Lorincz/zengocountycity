//
//  CityEditPopupWireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

typealias CityEditPopupCompletion = ((_ city: City?) -> ())?

protocol CityEditPopupRouter : class {
    func dismiss(city: City?)
}

class CityEditPopupWireframe : Wireframe {
    
    fileprivate let cityEditPopupViewController = CityEditPopupViewController()
    fileprivate var presenter: CityEditPopupPresenter?
    fileprivate let completion: CityEditPopupCompletion
    
    init(completion: CityEditPopupCompletion) {
        self.completion = completion
        presenter = CityEditPopupPresenter(view: cityEditPopupViewController, router: self)
        cityEditPopupViewController.eventHandler = presenter
    }
    
    func viewController() -> UIViewController {
        return cityEditPopupViewController
    }
}

//MARK: CityEditPopupRouter

extension CityEditPopupWireframe : CityEditPopupRouter {
    
    func dismiss(city: City?) {
        if let completion = completion {
            completion(city)
        }
    }
}

//MARK: PayloadHandler

extension CityEditPopupWireframe : PayloadHandler {
    
    func handlePayload(_ payload: Any?) {

        if let city = payload as? City {
            presenter?.city = city
        }

        if let county = payload as? County {
            presenter?.county = county
        }
    }
}

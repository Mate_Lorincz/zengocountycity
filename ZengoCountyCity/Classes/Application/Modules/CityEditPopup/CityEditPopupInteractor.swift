//
//  CityEditPopupInteractor.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CityEditPopupInteractorResult : class {
    func createActionFinishedWithResult(_ result: City)
    func createActionFinishedWithError(_ error: String)
    
    func modifyActionFinishedWithResult(_ result: City)
    func modifyActionFinishedWithError(_ error: String)
}

class CityEditPopupInteractor {
    weak var presenter: CityEditPopupInteractorResult?
    
    fileprivate var createCityService: CreateCityService? {
        get {
            return ServiceBinder.createCityService(self)
        }
    }

    fileprivate var modifyCityService: ModifyCityService? {
        get {
            return ServiceBinder.modifyCityService(self)
        }
    }

    func createCity(_ name: String, countyId: Int) {
        DispatchQueue.global().async { [unowned self] () -> Void in
            self.createCityService?.createCity(name, countyId: countyId)
        }
    }
    
    func modifyCity(_ name: String, cityId: Int) {
        DispatchQueue.global().async { [unowned self] () -> Void in
            self.modifyCityService?.modifyCity(name, cityId: cityId)
        }
    }
}

//MARK: CreateCityServiceDelegate

extension CityEditPopupInteractor : CreateCityServiceDelegate {
    
    func createCityServiceFinishedWithError(_ error: String) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.createActionFinishedWithError(error)
        })
    }
    
    func createCityServiceFinishedWithResult(_ result: City) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.createActionFinishedWithResult(result)
        })
    }
}

//MARK: ModifyCityServiceDelegate

extension CityEditPopupInteractor : ModifyCityServiceDelegate {
    
    func modifyCityServiceFinishedWithError(_ error: String) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.modifyActionFinishedWithError(error)
        })
    }
    
    func modifyCityServiceFinishedWithResult(_ result: City) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.modifyActionFinishedWithResult(result)
        })
    }
}

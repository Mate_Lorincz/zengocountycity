//
//  Wireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

protocol Wireframe : class {
    func viewController() -> UIViewController
}

protocol PayloadHandler : class {
    func handlePayload(_ payload: Any?)
}

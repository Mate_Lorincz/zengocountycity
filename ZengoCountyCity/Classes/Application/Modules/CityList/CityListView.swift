//
//  CityListView.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

protocol CityListView : class {
    var countyName: String? { get set }

    func presentPopup(_ popupViewController: UIViewController)
    func dismissPopup(_ popupViewController: UIViewController, completion: @escaping () -> ())
    func updateCityList(_ cityList: [City])
    func showAlert(_ message: String)
}

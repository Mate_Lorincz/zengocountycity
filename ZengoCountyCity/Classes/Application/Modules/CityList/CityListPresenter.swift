//
//  CityListPresenter.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CityListEventHandler : class {
    func createButtonTapped()
    func deleteButtonTapped(_ city: City)
    func editButtonTapped(_ city: City)
}

class CityListPresenter {
    
    fileprivate weak var router: CityListRouter?
    fileprivate let interactor = CityListInteractor()
    fileprivate var cityList: [City]?

    weak var view: CityListView?

    var county: County? {
        didSet {
            if let county = county {
                LoadingIndicator.presentLoadingIndicator()
                interactor.fetchCityList(county)
                view?.countyName = county.name
            }
        }
    }
    
    init (view: CityListView, router: CityListRouter) {
        self.view = view
        self.router = router
        interactor.presenter = self
    }
    
    func updateCity(city: City) {
        let updatedCity = cityList?.filter( { $0.id == city.id })

        if updatedCity?.isEmpty == true {
            cityList?.append(city)
            view?.updateCityList(cityList ?? [])
        }
        else {
            guard let county = county else {
                return
            }

            interactor.fetchCityList(county)
        }
    }
}

//MARK: CityListEventHandler

extension CityListPresenter : CityListEventHandler {
    
    func createButtonTapped() {
        router?.presentPopup(county)
    }
    
    func deleteButtonTapped(_ city: City) {
        LoadingIndicator.presentLoadingIndicator()
        interactor.deleteCity(city)
    }

    func editButtonTapped(_ city: City) {
        router?.presentPopup(city)
    }
}

//MARK: CityListInteractorResult

extension CityListPresenter : CityListInteractorResult {
    
    func fetchCityListFinishedWithError(_ error: String) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.showAlert(error)
    }
    
    func fetchCityListFinishedWithResult(_ result: [City]) {
        LoadingIndicator.dismissLoadingIndicator()
        cityList = result
        view?.updateCityList(result)
    }
    
    func deleteFinishedWithError(_ error: String) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.showAlert(error)
    }
    
    func deleteFinished() {

        guard let county = county else {
            return
        }
        
        interactor.fetchCityList(county)
    }
}

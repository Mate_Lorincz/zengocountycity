//
//  CityListWireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

protocol CityListRouter : class {
    func presentPopup(_ payLoad: Any?)
}

typealias CityListCompletion = (() -> ())?

class CityListWireframe {
    weak var router: MainRouter?
    
    fileprivate let cityListViewController = CityListViewController()
    fileprivate var presenter: CityListPresenter?
    fileprivate let completion: CityListCompletion
    fileprivate var cityEditPopupWireframe: CityEditPopupWireframe?
    
    init(completion: CityListCompletion) {
        self.completion = completion
        presenter = CityListPresenter(view: cityListViewController, router: self)
        cityListViewController.eventHandler = presenter
    }
}

//MARK: Wireframe

extension CityListWireframe : Wireframe {
    
    func viewController() -> UIViewController {
        return cityListViewController
    }
}

//MARK: CityListRouter

extension CityListWireframe : CityListRouter {
    
    func presentPopup(_ payload: Any?) {
        cityEditPopupWireframe = CityEditPopupWireframe() { (_ city: City?) -> () in
            
            if let city = city {
                self.presenter?.updateCity(city: city)
            }
            
            self.presenter?.view?.dismissPopup(self.cityEditPopupWireframe!.viewController(), completion: {})
        }
        if let payload = payload, let cityEditPopupWireframe = cityEditPopupWireframe {
            cityEditPopupWireframe.handlePayload(payload)
        }

        presenter?.view?.presentPopup(cityEditPopupWireframe!.viewController())
    }
}

//MARK: PayloadHandler

extension CityListWireframe : PayloadHandler {
    
    func handlePayload(_ payload: Any?) {
        
        if let county = payload as? County {
            presenter?.county = county
        }
    }
}

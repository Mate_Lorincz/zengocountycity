//
//  CityListInteractor.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CityListInteractorResult : class {
    func fetchCityListFinishedWithResult(_ result: [City])
    func fetchCityListFinishedWithError(_ error: String)
    func deleteFinished()
    func deleteFinishedWithError(_ error: String)
}

class CityListInteractor {
    weak var presenter: CityListInteractorResult?
    
    fileprivate var fetchCityListService: FetchCityListService? {
        get {
            return ServiceBinder.fetchCityListService(self)
        }
    }

    fileprivate var deleteCityService: DeleteCityService? {
        get {
            return ServiceBinder.deleteCityService(self)
        }
    }

    func fetchCityList(_ county: County) {
        DispatchQueue.global().async { [unowned self] () -> Void in
            self.fetchCityListService?.fetchCityList(county.id)
        }
    }
    
    func deleteCity(_ city: City) {
        DispatchQueue.global().async { [unowned self] () -> Void in
            self.deleteCityService?.deleteCity(city.id)
        }
    }
}

//MARK: FetchCityListServiceDelegate

extension CityListInteractor : FetchCityListServiceDelegate {
    
    func fetchCityListServiceFinishedWithError(_ error: String) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.fetchCityListFinishedWithError(error)
        })
    }
    
    func fetchCityListServiceFinishedWithResult(_ result: [City]) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.fetchCityListFinishedWithResult(result)
        })
    }
}

//MARK: DeleteCityServiceDelegate

extension CityListInteractor : DeleteCityServiceDelegate {
    
    func deleteCityServiceFinishedWithError(_ error: String) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.deleteFinishedWithError(error)
        })
    }
    
    func deleteCityServiceFinished() {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.deleteFinished()
        })
    }
}

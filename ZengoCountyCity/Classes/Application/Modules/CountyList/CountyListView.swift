//
//  CountyListView.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CountyListView : class {
    func updateCountyList(_ countyList: [County])
    func showAlert(_ message: String)
}

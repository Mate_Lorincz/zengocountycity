//
//  CountyListInteractor.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CountyListInteractorResult : class {
    func fetchCountyListFinishedWithResult(_ result: [County])
    func fetchCountyListFinishedWithError(_ error: String)
}

class CountyListInteractor {
    
    weak var presenter: CountyListInteractorResult?
    
    fileprivate var fetchCountyListService: FetchCountyListService? {
        get {
            return ServiceBinder.fetchCountyListService(self)
        }
    }
    
    func fetchCountyList() {
        DispatchQueue.global().async { [unowned self] () -> Void in
            self.fetchCountyListService?.fetchCountyList()
        }
    }
}

//MARK: FetchCountyListServiceDelegate

extension CountyListInteractor : FetchCountyListServiceDelegate {
    
    func fetchCountyListServiceFinishedWithError(_ error: String) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.fetchCountyListFinishedWithError(error)
        })
    }
    
    func fetchCountyListServiceFinishedWithResult(_ result: [County]) {
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.presenter?.fetchCountyListFinishedWithResult(result)
        })
    }
}

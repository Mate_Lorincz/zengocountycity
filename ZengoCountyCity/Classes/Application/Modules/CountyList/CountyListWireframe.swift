//
//  CountyListWireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

protocol CountyListRouter : class {
    func pushCityListScreen(_ county: County)
}

typealias CountyListCompletion = (() -> ())?

class CountyListWireframe {
    
    weak var router: MainRouter?
    
    fileprivate let countyListViewController = CountyListViewController()
    fileprivate var presenter: CountyListPresenter?
    fileprivate let completion: CountyListCompletion
    
    init(completion: CountyListCompletion) {
        self.completion = completion
        presenter = CountyListPresenter(view: countyListViewController, router: self)
        countyListViewController.eventHandler = presenter
    }
}

//MARK: Wireframe

extension CountyListWireframe : Wireframe {
    
    func viewController() -> UIViewController {
        return countyListViewController
    }
}

//MARK: CountyListRouter

extension CountyListWireframe : CountyListRouter {
    
    func pushCityListScreen(_ county: County) {
        UIViewController.setBackButton("", target: self.viewController())
        router?.pushScreen(.CityList, withNavigationController: self.viewController().navigationController!, payload: county)
    }
}

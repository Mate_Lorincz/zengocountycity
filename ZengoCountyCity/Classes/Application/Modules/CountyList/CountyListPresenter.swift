//
//  CountyListPresenter.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CountyListEventHandler : class {
    func countySelected(county: County)
}

class CountyListPresenter {

    fileprivate weak var view: CountyListView?
    fileprivate weak var router: CountyListRouter?
    fileprivate let interactor = CountyListInteractor()

    init (view: CountyListView, router: CountyListRouter) {
        self.view = view
        self.router = router
        interactor.presenter = self
        LoadingIndicator.presentLoadingIndicator()
        interactor.fetchCountyList()
    }
}

//MARK: CountyListEventHandler

extension CountyListPresenter : CountyListEventHandler {
    
    func countySelected(county: County) {
        router?.pushCityListScreen(county)
    }
}

//MARK: CountyListInteractorResult

extension CountyListPresenter : CountyListInteractorResult {
    
    func fetchCountyListFinishedWithError(_ error: String) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.showAlert(error)
    }
    
    func fetchCountyListFinishedWithResult(_ result: [County]) {
        LoadingIndicator.dismissLoadingIndicator()
        view?.updateCountyList(result)
    }
}

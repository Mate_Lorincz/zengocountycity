//
//  SplashWireframe.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

protocol SplashRouter : class {
    func displayLandingScreen()
}

typealias SplashCompletion = () -> ()

class SplashWireframe {
    
    fileprivate let splashViewController = SplashViewController()
    fileprivate var presenter: SplashPresenter?
    fileprivate let completion: SplashCompletion
    
    init(completion: @escaping SplashCompletion) {
        self.completion = completion
        presenter = SplashPresenter(view: splashViewController, router: self)
    }
}

//MARK: Wireframe

extension SplashWireframe : Wireframe {
    
    func viewController() -> UIViewController {
        return splashViewController
    }
}

//MARK: SplashRouter

extension SplashWireframe : SplashRouter {
    
    func displayLandingScreen() {
        completion()
    }
}

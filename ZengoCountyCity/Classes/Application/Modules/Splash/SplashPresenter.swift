//
//  SplashPresenter.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

class SplashPresenter {
    fileprivate weak var view: SplashView?
    fileprivate weak var router: SplashRouter?
    
    init (view: SplashView, router: SplashRouter) {
        self.view = view
        self.router = router
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            self.router?.displayLandingScreen()
        }
    }
}

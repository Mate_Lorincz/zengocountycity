//
//  TriangleView.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 07..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class TriangleView : UIView {
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        context.closePath()
        
        context.setFillColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        context.fillPath()
    }
}

//
//  CityTableViewCell.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class CityTableViewCell : UITableViewCell {
    
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet weak var triangleView: TriangleView!
    
    func configure(_ city: City?) {
        
        if let city = city {
            nameLabel.text = city.name
        }
    }
}

//
//  CountyTableViewCell.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class CountyTableViewCell : UITableViewCell {
    
    @IBOutlet fileprivate weak var countyImageView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    
    func configure(_ county: County?) {
        
        if let county = county {
            nameLabel?.text = county.name
            countyImageView?.image = UIImage(named: "county_\(county.id)")
        }
    }
}

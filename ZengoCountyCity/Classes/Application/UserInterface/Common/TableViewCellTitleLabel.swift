//
//  TableViewCellTitleLabel.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class TableViewCellTitleLabel : UILabel {
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0.0, left: 10.0, bottom: 0.0, right: 48.0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}

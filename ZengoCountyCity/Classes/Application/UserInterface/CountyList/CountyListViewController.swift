//
//  CountyListViewController.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class CountyListViewController : UIViewController {
    
    weak var eventHandler: CountyListEventHandler?

    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate var countyList = [County]()

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationBar()
        registerCellNibs()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

//MARK: CountyListViewController (private)

private extension CountyListViewController {

    func customizeNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.appBlackColor()
        navigationItem.applyNavigationItem(withImageName: "zen_z")
    }
    
    func registerCellNibs() {
        let countyTableViewCellNibName = NSStringFromClass(CountyTableViewCell.self)
        let countyTableViewCellNib = UINib(nibName:countyTableViewCellNibName.components(separatedBy: ".").last!, bundle: nil)
        tableView?.register(countyTableViewCellNib, forCellReuseIdentifier:countyTableViewCellNibName)
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource

extension CountyListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CountyTableViewCell.self), for: indexPath) as! CountyTableViewCell
        cell.configure(countyList[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventHandler?.countySelected(county: countyList[indexPath.row])
    }
}

//MARK: CountyListView

extension CountyListViewController : CountyListView {
    
    func showAlert(_ message: String) {
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: .none))
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateCountyList(_ countyList: [County]) {
        self.countyList = countyList
        tableView?.reloadData()
        tableView?.scrollRectToVisible(CGRect(x: 0.0,  y: 0.0, width: 1.0, height: 1.0), animated: true)
    }
}

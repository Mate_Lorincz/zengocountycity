//
//  CityEditPopupViewController.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class CityEditPopupViewController : UIViewController {

    var cityName: String? {
        didSet {
            if let cityName = cityName {
                updateWithCityName(cityName)
            }
        }
    }

    weak var eventHandler: CityEditPopupEventHandler?
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var popupTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popupView: UIView!
    fileprivate var topConstraintOriginalConstant: CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUILayout()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if cityNameTextField.isEditing {
            hideKeyboard()
        }
        else {
            if let touchedView = touches.first?.view {
                if touchedView == self.view {
                    eventHandler?.dismiss()
                }
            }
        }
    }
    
    @IBAction func actonButtonTapped() {
        eventHandler?.actionButtonTapped(cityName: cityNameTextField?.text ?? "")
    }
    
    @IBAction func cancelButtonTapped() {
        eventHandler?.dismiss()
    }
}

//MARK: CityEditPopupViewController (private)

private extension CityEditPopupViewController {
    
    func setupUILayout() {
        topConstraintOriginalConstant = self.view.frame.size.height / 2.0 - popupView.frame.size.height / 2.0
        popupTopConstraint.constant = topConstraintOriginalConstant ?? 0.0
        popupView.applyShadow(CGSize(width: 5, height: 5))
        
        if let cityName = cityName {
            updateWithCityName(cityName)
        }
    }
    
    func updateWithCityName(_ cityName: String) {
        actionLabel?.text = "Edit city"
        cityNameTextField?.text = cityName
        actionButton?.setTitle("OK", for: .normal)
    }
    
    func hideKeyboard() {
        popupTopConstraint.constant = topConstraintOriginalConstant ?? 0.0
        cityNameTextField.resignFirstResponder()
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: CityEditPopupView

extension CityEditPopupViewController : CityEditPopupView {
    
    func showAlert(_ message: String) {
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: .none))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: UITextFieldDelegate

extension CityEditPopupViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        popupTopConstraint.constant = self.view.frame.size.height / 4.0

        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        
        return true
    }
}

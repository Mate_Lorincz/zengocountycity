//
//  LoadingIndicator.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

private let loadingIndicator = LoadingIndicator()

class LoadingIndicatorView : UIView {
    
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
}

class LoadingIndicator {
    
    fileprivate var isPresenting = false
    fileprivate let loadingIndicatorView : LoadingIndicatorView?
    fileprivate let keyWindow : UIWindow = UIApplication.shared.keyWindow!
    
    init() {
        let loadingIndicatorViewNibName = NSStringFromClass(LoadingIndicatorView.self)
        loadingIndicatorView = UINib(nibName: loadingIndicatorViewNibName.components(separatedBy: ".").last!, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? LoadingIndicatorView
    }
    
    static func presentLoadingIndicator() {
        
        if loadingIndicator.isPresenting {
            return
        }
        
        loadingIndicator.presentLoadingIndicator()
    }
    
    static func dismissLoadingIndicator() {
        
        if loadingIndicator.isPresenting {
            loadingIndicator.dismissLoadingIndicator()
        }
    }
}

//MARK: LoadingIndicator (private)

private extension LoadingIndicator {
    
    func presentLoadingIndicator() {
        isPresenting = true
        
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            if let indicatorView = self.loadingIndicatorView {
                indicatorView.frame = self.keyWindow.bounds
                indicatorView.loadingIndicatorView.startAnimating()
                self.keyWindow.addSubview(indicatorView)
                self.keyWindow.bringSubview(toFront: indicatorView)
            }
        })
    }
    
    func dismissLoadingIndicator() {
        
        DispatchQueue.main.async(execute: { [unowned self] () -> Void in
            self.loadingIndicatorView?.loadingIndicatorView.stopAnimating()
            self.loadingIndicatorView?.removeFromSuperview()
            self.isPresenting = false
        })
    }
}

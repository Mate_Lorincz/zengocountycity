//
//  CityListViewController.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class CityListViewController : UIViewController {
    
    var countyName: String? {
        didSet {
            self.title = countyName
        }
    }
    
    weak var eventHandler: CityListEventHandler?
    @IBOutlet fileprivate weak var createButton: UIButton!
    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate var cityList = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationBar()
        customizeUILayout()
        registerCellNibs()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func createCityButtonTapped() {
        eventHandler?.createButtonTapped()
    }
}

//MARK: CityListViewController (private)

private extension CityListViewController {
    
    func customizeUILayout() {
        createButton.layer.cornerRadius = createButton.frame.size.height / 2.0
        createButton.applyShadow(CGSize(width: 2.0, height: 2.0))
    }

    func customizeNavigationBar() {
        self.title = countyName
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.applicationBoldFontWithSize(22.0), NSForegroundColorAttributeName : UIColor.appWhiteColor()]
    }
    
    func registerCellNibs() {
        let cityTableViewCellNibName = NSStringFromClass(CityTableViewCell.self)
        let cityTableViewCellNib = UINib(nibName:cityTableViewCellNibName.components(separatedBy: ".").last!, bundle: nil)
        tableView?.register(cityTableViewCellNib, forCellReuseIdentifier:cityTableViewCellNibName)

        let emptyCityListTableViewCellNibName = NSStringFromClass(EmptyCityListTableViewCell.self)
        let emptyCityListTableViewCellNib = UINib(nibName:emptyCityListTableViewCellNibName.components(separatedBy: ".").last!, bundle: nil)
        tableView?.register(emptyCityListTableViewCellNib, forCellReuseIdentifier:emptyCityListTableViewCellNibName)
    }
    
    func showPopupViewController(_ viewController: UIViewController) {
        self.addChildViewController(viewController)
        viewController.view.frame = self.view.bounds
        self.view.addSubview(viewController.view)
        viewController.view.alpha = 0.0
        viewController.didMove(toParentViewController: self)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            viewController.view.alpha = 1.0
        })
    }
    
    func hidePopupViewController(_ viewController: UIViewController, completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            viewController.view.alpha = 0.0
        }, completion: { (_) -> Void in
            viewController.willMove(toParentViewController: .none)
            viewController.removeFromParentViewController()
            viewController.view.removeFromSuperview()
            completion()
        })
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource

extension CityListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cityList.isEmpty ? 120.0 : 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityList.isEmpty ? 1 : cityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if cityList.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(EmptyCityListTableViewCell.self), for: indexPath) as! EmptyCityListTableViewCell

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CityTableViewCell.self), for: indexPath) as! CityTableViewCell
        cell.configure(cityList[indexPath.row])
        cell.triangleView.backgroundColor = [UIColor.appYellowColor(), UIColor.appGreenColor(), UIColor.appRedColor(), UIColor.appBlueColor(), UIColor.appPurpleColor()][indexPath.row % 5]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cityList.indices.contains(indexPath.row) {
            eventHandler?.editButtonTapped(cityList[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "delete") { action, index in
            self.tableView(tableView, commit: UITableViewCellEditingStyle.delete, forRowAt: indexPath)
        }
        
        deleteButton.backgroundColor = [UIColor.appYellowColor(), UIColor.appGreenColor(), UIColor.appRedColor(), UIColor.appBlueColor(), UIColor.appPurpleColor()][indexPath.row % 5]
        
        return [deleteButton]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return cityList.isEmpty == false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            eventHandler?.deleteButtonTapped(cityList[indexPath.row])
            cityList.remove(at: indexPath.row)
            
            if cityList.isEmpty {
                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            else {
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.createButton.alpha = 0.0
        })
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.createButton.alpha = 1.0
        })
    }
}

//MARK: CityListView

extension CityListViewController : CityListView {
    
    func showAlert(_ message: String) {
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: .none))
        self.present(alert, animated: true, completion: nil)
    }

    func updateCityList(_ cityList: [City]) {
        self.cityList = cityList.sorted(by: {$0.name < $1.name})
        tableView?.reloadData()
        tableView?.scrollRectToVisible(CGRect(x: 0.0,  y: 0.0, width: 1.0, height: 1.0), animated: true)
    }
    
    func presentPopup(_ popupViewController: UIViewController) {
        self.showPopupViewController(popupViewController)
    }
    
    func dismissPopup(_ popupViewController: UIViewController, completion: @escaping () -> ()) {
        self.hidePopupViewController(popupViewController) { () -> () in
            completion()
        }
    }
}

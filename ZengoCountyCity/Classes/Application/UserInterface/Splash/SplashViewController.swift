//
//  SplashViewController.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import UIKit

class SplashViewController : UIViewController {
    
    @IBOutlet fileprivate weak var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeLoadingview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showLoadingView()
    }
}

//MARK: SplashViewController (private)

private extension SplashViewController {
    
    func customizeLoadingview() {
        loadingView.layer.cornerRadius = 5.0
        loadingView.alpha = 0.0
    }
    
    func showLoadingView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.loadingView.alpha = 1.0
        })
    }
}

//MARK: SplashView

extension SplashViewController : SplashView {
    
}

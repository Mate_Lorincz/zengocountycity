//
//  ModifyCityService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol ModifyCityServiceDelegate : class {
    func modifyCityServiceFinishedWithResult(_ result: City)
    func modifyCityServiceFinishedWithError(_ error: String)
}

class ModifyCityService {
    
    weak var delegate: ModifyCityServiceDelegate?
    
    init(delegate: ModifyCityServiceDelegate) {
        self.delegate = delegate
    }

    func modifyCity(_ name: String, cityId: Int) {
        if ReachabilityService.isNetworkReachable == false {
            self.delegate?.modifyCityServiceFinishedWithError("No internet connection")
            return
        }
        
        let validatedName = name.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? name
        
        guard let urlString = URLProvider.cityModifyURLString(validatedName, cityId: cityId), let url = URL(string: urlString) else {
            self.delegate?.modifyCityServiceFinishedWithError("Couldn't resolve service endpoint")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PATCH"
        request.addValue(URLProvider.token, forHTTPHeaderField: "token")
        request.timeoutInterval = DefaultTimeoutInterval
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if let _ = error {
                self.delegate?.modifyCityServiceFinishedWithError(error.debugDescription)
                return
            }
            else {
                guard let data = data else {
                    self.delegate?.modifyCityServiceFinishedWithError("No data")
                    return
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String : AnyObject] {
                        let apiServiceResponse = ServiceResponseManager.apiServiceResponseFromJSONResult(jsonResult)
                        
                        if apiServiceResponse.success {
                            self.delegate?.modifyCityServiceFinishedWithResult(ServiceResponseManager.cityFromResultData(jsonResult as AnyObject))
                        }
                        else if let errorMessage = apiServiceResponse.errorMessage {
                            self.delegate?.modifyCityServiceFinishedWithError(errorMessage)
                        }
                        else {
                            self.delegate?.modifyCityServiceFinishedWithError("Unknown error")
                        }
                    }
                }
                catch _ as NSError {
                    self.delegate?.modifyCityServiceFinishedWithError(error.debugDescription)
                    return
                }
            }
        }
        
        task.resume()
    }
}

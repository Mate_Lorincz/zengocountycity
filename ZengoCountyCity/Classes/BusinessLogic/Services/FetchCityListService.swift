//
//  FetchCityListService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol FetchCityListServiceDelegate : class {
    func fetchCityListServiceFinishedWithResult(_ result: [City])
    func fetchCityListServiceFinishedWithError(_ error: String)
}

class FetchCityListService {
    
    weak var delegate: FetchCityListServiceDelegate?
    
    init(delegate: FetchCityListServiceDelegate) {
        self.delegate = delegate
    }
    
    func fetchCityList(_ countyId: Int) {
        
        if ReachabilityService.isNetworkReachable == false {
            self.delegate?.fetchCityListServiceFinishedWithError("No internet connection")
            return
        }
        
        guard let urlString = URLProvider.cityListURLString(countyId), let url = URL(string: urlString) else {
            self.delegate?.fetchCityListServiceFinishedWithError("Couldn't resolve service endpoint")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue(URLProvider.token, forHTTPHeaderField: "token")
        request.timeoutInterval = DefaultTimeoutInterval
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if let _ = error {
                self.delegate?.fetchCityListServiceFinishedWithError(error.debugDescription)
            }
            else {
                guard let data = data else {
                    self.delegate?.fetchCityListServiceFinishedWithError("No data")
                    return
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String : AnyObject] {
                        let apiServiceResponse = ServiceResponseManager.apiServiceResponseFromJSONResult(jsonResult)
                        
                        if apiServiceResponse.success {
                            self.delegate?.fetchCityListServiceFinishedWithResult(ServiceResponseManager.cityListFromResultData(jsonResult as AnyObject))
                        }
                        else if let errorMessage = apiServiceResponse.errorMessage {
                            self.delegate?.fetchCityListServiceFinishedWithError(errorMessage)
                        }
                        else {
                            self.delegate?.fetchCityListServiceFinishedWithError("Unknown error")
                        }
                    }
                }
                catch _ as NSError {
                    self.delegate?.fetchCityListServiceFinishedWithError(error.debugDescription)
                }
            }
        }
        
        task.resume()
    }
}

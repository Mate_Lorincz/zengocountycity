//
//  ServiceResponseManager.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

class ServiceResponseManager {
    
    static func apiServiceResponseFromJSONResult(_ jsonResult: [String : AnyObject]) -> APIServiceResponse {
        return APIServiceResponse(success: jsonResult["success"] as? Bool ?? false, errorCode: jsonResult["errorCode"] as? Int, errorMessage: (jsonResult["errorMessage"]!["name"] as? [String])?.first, data: jsonResult["data"])
    }
    
    static func countyListFromResultData(_ resultData: AnyObject) -> [County] {
        var countyArray: [County] = []
        
        if let data = resultData["data"] as? [AnyObject] {
            for countyData in data {
                if let id = countyData["id"] as? Int, let name = countyData["name"] as? String {
                    let validName = name.replacingOccurrences(of: " megye", with: "")
                    countyArray.append(County(name: validName, id: id))
                }
            }
        }
        
        return countyArray
    }
    
    static func cityListFromResultData(_ resultData: AnyObject) -> [City] {
        var cityArray: [City] = []
        
        if let data = resultData["data"] as? [AnyObject] {
            for cityData in data {
                if let id = cityData["id"] as? Int, let name = cityData["name"] as? String {
                    cityArray.append(City(name: name, id: id))
                }
            }
        }
        
        return cityArray
    }

    static func cityFromResultData(_ resultData: AnyObject) -> City {
        var city: City = City(name: "", id: 0)
        
        if let data = resultData["data"] as? [String : AnyObject] {
            if let id = data["id"] as? Int, let name = data["name"] as? String {
                city = City(name: name, id: id)
            }
        }
        
        return city
    }
}

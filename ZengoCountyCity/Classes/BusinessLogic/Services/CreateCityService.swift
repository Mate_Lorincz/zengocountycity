//
//  CreateCityService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol CreateCityServiceDelegate : class {
    func createCityServiceFinishedWithResult(_ result: City)
    func createCityServiceFinishedWithError(_ error: String)
}

class CreateCityService {
    
    weak var delegate: CreateCityServiceDelegate?
    
    init(delegate: CreateCityServiceDelegate) {
        self.delegate = delegate
    }
    
    func createCity(_ name: String, countyId: Int) {
        
        if ReachabilityService.isNetworkReachable == false {
            self.delegate?.createCityServiceFinishedWithError("No internet connection")
            return
        }
        
        let validatedName = name.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? name
        
        guard let urlString = URLProvider.cityCreateURLString(validatedName, countyId: countyId), let url = URL(string: urlString) else {
            self.delegate?.createCityServiceFinishedWithError("Couldn't resolve service endpoint")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.addValue(URLProvider.token, forHTTPHeaderField: "token")
        request.timeoutInterval = DefaultTimeoutInterval
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if let _ = error {
                self.delegate?.createCityServiceFinishedWithError(error.debugDescription)
                return
            }
            else {
                guard let data = data else {
                    self.delegate?.createCityServiceFinishedWithError("No data")
                    return
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String : AnyObject] {
                        let apiServiceResponse = ServiceResponseManager.apiServiceResponseFromJSONResult(jsonResult)
                        
                        if apiServiceResponse.success {
                            self.delegate?.createCityServiceFinishedWithResult(ServiceResponseManager.cityFromResultData(jsonResult as AnyObject))
                        }
                        else if let errorMessage = apiServiceResponse.errorMessage {
                            self.delegate?.createCityServiceFinishedWithError(errorMessage)
                        }
                        else {
                            self.delegate?.createCityServiceFinishedWithError("Unknown error")
                        }
                    }
                }
                catch _ as NSError {
                    self.delegate?.createCityServiceFinishedWithError(error.debugDescription)
                    return
                }
            }
        }
        
        task.resume()
    }
}

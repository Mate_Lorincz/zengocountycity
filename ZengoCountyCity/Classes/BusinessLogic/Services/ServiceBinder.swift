//
//  ServiceBinder.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

let DefaultTimeoutInterval = 20.0

class ServiceBinder {
    
    static func fetchCountyListService(_ delegate : FetchCountyListServiceDelegate) -> FetchCountyListService {
        return FetchCountyListService(delegate: delegate)
    }

    static func fetchCityListService(_ delegate : FetchCityListServiceDelegate) -> FetchCityListService {
        return FetchCityListService(delegate: delegate)
    }

    static func createCityService(_ delegate : CreateCityServiceDelegate) -> CreateCityService {
        return CreateCityService(delegate: delegate)
    }

    static func modifyCityService(_ delegate : ModifyCityServiceDelegate) -> ModifyCityService {
        return ModifyCityService(delegate: delegate)
    }

    static func deleteCityService(_ delegate : DeleteCityServiceDelegate) -> DeleteCityService {
        return DeleteCityService(delegate: delegate)
    }
}

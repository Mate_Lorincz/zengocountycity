//
//  DeleteCityService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol DeleteCityServiceDelegate : class {
    func deleteCityServiceFinished()
    func deleteCityServiceFinishedWithError(_ error: String)
}

class DeleteCityService {
    
    weak var delegate: DeleteCityServiceDelegate?
    
    init(delegate: DeleteCityServiceDelegate) {
        self.delegate = delegate
    }
    
    func deleteCity(_ cityId: Int) {
        
        if ReachabilityService.isNetworkReachable == false {
            self.delegate?.deleteCityServiceFinishedWithError("No internet connection")
            return
        }
        
        guard let urlString = URLProvider.cityDeleteURLString(cityId), let url = URL(string: urlString) else {
            self.delegate?.deleteCityServiceFinishedWithError("Couldn't resolve service endpoint")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue(URLProvider.token, forHTTPHeaderField: "token")
        request.timeoutInterval = DefaultTimeoutInterval
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if let _ = error {
                self.delegate?.deleteCityServiceFinishedWithError(error.debugDescription)
                return
            }
            else {
                guard let data = data else {
                    self.delegate?.deleteCityServiceFinishedWithError("No data")
                    return
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String : AnyObject] {
                        let apiServiceResponse = ServiceResponseManager.apiServiceResponseFromJSONResult(jsonResult)
                        
                        if apiServiceResponse.success {
                            self.delegate?.deleteCityServiceFinished()
                        }
                        else if let errorMessage = apiServiceResponse.errorMessage {
                            self.delegate?.deleteCityServiceFinishedWithError(errorMessage)
                        }
                        else {
                            self.delegate?.deleteCityServiceFinishedWithError("Unknown error")
                        }
                    }
                }
                catch _ as NSError {
                    self.delegate?.deleteCityServiceFinishedWithError(error.debugDescription)
                    return
                }
            }
        }
        
        task.resume()
    }
}

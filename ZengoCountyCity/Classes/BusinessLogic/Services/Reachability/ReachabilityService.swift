//
//  ReachabilityService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

struct ReachabilityService {
    
    static var isNetworkReachable: Bool {
        get {
            let url: URL = URL(string: "http://google.com" as String)!
            let reachability = Reachability(hostname: url.host!)
            
            return reachability?.currentReachabilityStatus != .notReachable
        }
    }
}

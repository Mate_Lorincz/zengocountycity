//
//  FetchCountyListService.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 08..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

protocol FetchCountyListServiceDelegate : class {
    func fetchCountyListServiceFinishedWithResult(_ result: [County])
    func fetchCountyListServiceFinishedWithError(_ error: String)
}

class FetchCountyListService {
    
    weak var delegate: FetchCountyListServiceDelegate?

    init(delegate: FetchCountyListServiceDelegate) {
        self.delegate = delegate
    }

    func fetchCountyList() {
        
        guard let urlString = URLProvider.countyListURLString(), let url = URL(string: urlString) else {
            self.fetchCountyListCachedResponse()
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(URLProvider.token, forHTTPHeaderField: "token")
        request.timeoutInterval = DefaultTimeoutInterval
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if let _ = error {
                self.fetchCountyListCachedResponse()
            }
            else {
                guard let data = data else {
                    self.fetchCountyListCachedResponse()
                    return
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String : AnyObject] {
                        let apiServiceResponse = ServiceResponseManager.apiServiceResponseFromJSONResult(jsonResult)
                        
                        if apiServiceResponse.success {
                            self.delegate?.fetchCountyListServiceFinishedWithResult(ServiceResponseManager.countyListFromResultData(jsonResult as AnyObject))
                        }
                        else if let errorMessage = apiServiceResponse.errorMessage {
                            self.delegate?.fetchCountyListServiceFinishedWithError(errorMessage)
                        }
                        else {
                            self.delegate?.fetchCountyListServiceFinishedWithError("Unknown error")
                        }
                    }
                }
                catch _ as NSError {
                    self.fetchCountyListCachedResponse()
                }
            }
        }
        
        task.resume()
    }
    
    func fetchCountyListCachedResponse() {
        
        if let path = Bundle.main.path(forResource: "all_states", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options:.allowFragments)
                delegate?.fetchCountyListServiceFinishedWithResult(ServiceResponseManager.countyListFromResultData(jsonResult as AnyObject))
            }
            catch let error as NSError {
                delegate?.fetchCountyListServiceFinishedWithError(error.localizedDescription)
            }
        }
        else {
            delegate?.fetchCountyListServiceFinishedWithError("Invalid filename/path")
        }
    }
}

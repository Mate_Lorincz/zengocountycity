//
//  URLProvider.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 05..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

struct URLProvider {
    
    static let token = "2f9130922c63fa2d6505ab958f2385da"
    
    // base URL
    fileprivate static let zengoBaseURL = "https://probafeladat-api.zengo.eu/api"
    
    // API Endpoints
    fileprivate static let countyListEndpoint = "/all_states"
    fileprivate static let cityListEndpoint = "/state_city"
    fileprivate static let cityEndpoint = "/city"

    static func countyListURLString() -> String? {
        return zengoBaseURL + countyListEndpoint
    }
    
    static func cityListURLString(_ countyId: Int) -> String? {
        return zengoBaseURL + cityListEndpoint + "?state_id=\(countyId)"
    }

    static func cityCreateURLString(_ cityName: String, countyId: Int) -> String? {
        return zengoBaseURL + cityEndpoint + "?name=\(cityName)&state_id=\(countyId)"
    }
    
    static func cityModifyURLString(_ cityName: String, cityId: Int) -> String? {
        return zengoBaseURL + cityEndpoint + "?name=\(cityName)&city_id=\(cityId)"
    }
    
    static func cityDeleteURLString(_ cityId: Int) -> String? {
        return zengoBaseURL + cityEndpoint + "?city_id=\(cityId)"
    }
}

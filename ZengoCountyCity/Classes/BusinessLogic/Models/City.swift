//
//  City.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 06..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

struct City {
    
    var name: String
    var id: Int
}

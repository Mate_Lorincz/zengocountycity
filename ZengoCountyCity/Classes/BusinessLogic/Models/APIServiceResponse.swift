//
//  APIServiceResponse.swift
//  ZengoCountyCity
//
//  Created by Mate Lorincz on 2017. 05. 07..
//  Copyright © 2017. MateLorincz. All rights reserved.
//

import Foundation

struct APIServiceResponse {
    
    var success: Bool = false
    var errorCode: Int?
    var errorMessage: String?
    var data: Any?
}
